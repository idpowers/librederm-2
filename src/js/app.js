import mainSlider from './components/mainSlider'
import colorSlider from './components/cards/colorSlider'
import productSlider from './components/product/'
import floatlabel from './components/floatlabel'
import pageSlider from './components/page'
import modalInit from './components/modal'

document.addEventListener('DOMContentLoaded', function () {
  mainSlider()
  colorSlider()
  productSlider()
  floatlabel()
  pageSlider()
  modalInit()
})
